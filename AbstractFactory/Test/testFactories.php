<?php
namespace shuravinGR\skillUp\AbstractFactory\Test;

use shuravinGR\skillUp\AbstractFactory\AbstractFactory;
use shuravinGR\skillUp\AbstractFactory\Product1;
use shuravinGR\skillUp\AbstractFactory\Product2;

require_once(__DIR__ . '/../AbstractFactory.php');
require_once(__DIR__ . '/../Factory1.php');
require_once(__DIR__ . '/../Factory2.php');
require_once(__DIR__ . '/../Product1.php');
require_once(__DIR__ . '/../Product2.php');

$factory = AbstractFactory::getFactory(Product1::class);
if ($factory !== null) {
    $factory->getFactoryProduct()->saySomething();
}

$factory = AbstractFactory::getFactory('product34');
if ($factory === null) {
    echo "factory34 does not exists\n";
}

$factory = AbstractFactory::getFactory(Product2::class);
if ($factory !== null) {
    $factory->getFactoryProduct()->saySomething();
}

$object = AbstractFactory::getOgject();