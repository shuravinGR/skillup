<?php

namespace shuravinGR\skillUp\AbstractFactory;

abstract class AbstractFactory
{
    public const FACTORIES_KEYS = [
        Product1::class => Factory1::class,
        Product2::class => Factory2::class
    ];

    /**
     * @param string $factoryKey
     * @return AbstractFactory|null
     */
    public static function getFactory(string $factoryKey): ?AbstractFactory
    {
        $factory = self::FACTORIES_KEYS[$factoryKey] ?? null;
        return $factory === null ? null : new $factory();
    }

    abstract public function getFactoryProduct();

    static public function getOgject()
    {
        return new Product1();
    }
}