<?php

namespace shuravinGR\skillUp\Facade;

use shuravinGR\skillUp\Facade\Car\Dashboard;
use shuravinGR\skillUp\Facade\Car\Engine;
use shuravinGR\skillUp\Facade\Car\Lights;

class Car
{
    private Dashboard $dashboard;

    private Engine $engine;

    private Lights $lights;

    public function __construct()
    {
        $this->dashboard = new Dashboard();
        $this->engine = new Engine();
        $this->lights = new Lights();
    }

    public function start()
    {
        echo "Car starting...\n";
        $this->engine->startEngine();
        $this->dashboard->turnOnDashboard();
        $this->lights->turnOnTheLights();
        echo "Car started successfully!\n";
    }
}