<?php

namespace shuravinGR\skillUp\Facade\Car;

class Lights
{
    public function turnOnTheLights()
    {
        echo "Lights turned on\n";
    }
}