<?php

namespace shuravinGR\skillUp\Facade\Car;

class Dashboard
{
    public function turnOnDashboard()
    {
        echo "Dashboard turned on\n";
    }
}