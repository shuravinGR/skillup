<?php

namespace shuravinGR\skillUp\Facade\Car;

class Engine
{
    public function startEngine()
    {
        echo "Engine started\n";
    }
}