<?php

use shuravinGR\skillUp\Facade\Car;

require_once(__DIR__ . '/../Car.php');
require_once(__DIR__ . '/../Car/Dashboard.php');
require_once(__DIR__ . '/../Car/Engine.php');
require_once(__DIR__ . '/../Car/Lights.php');

$car = new Car();
$car->start();