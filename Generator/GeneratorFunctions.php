<?php

namespace Generator;

use Generator;

class GeneratorFunctions
{
    public function getNumbers(): Generator
    {
        for ($i = 0; $i <= 10; $i++) yield $i;
    }

    public function getNumbersWithYieldFrom(): Generator
    {
        for ($i = 0; $i <= 5; $i++) yield $i;
        yield from [6,7,8];
        yield 9;
        yield 10;
    }

    public function echoSend(): Generator
    {
        while (true) {
            echo yield . "\n";
        }
    }
}