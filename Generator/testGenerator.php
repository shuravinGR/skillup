<?php

namespace Generator;

require_once 'GeneratorFunctions.php';

echo "test 1\n";
$functions = new GeneratorFunctions();
foreach ($functions->getNumbers() as $number) {
    echo "$number\n";
}

echo "test 2\n";
$functions = new GeneratorFunctions();
$numbers = $functions->getNumbers();
$numbers->next();
$numbers->next();
echo $numbers->current() . "\n";
$numbers->next();
echo $numbers->key() . "\n";
$numbers->next();
echo $numbers->current() . "\n";

echo "test 3\n";
foreach ($functions->getNumbersWithYieldFrom() as $value) {
    echo "$value\n";
}

echo "test 4\n";
$generator = $functions->echoSend();
$generator->send('testValue');
$generator->send('testValue2');
$generator->send('testValue3');