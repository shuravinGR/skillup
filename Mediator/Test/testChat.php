<?php
namespace shuravinGR\SkillUp\Mediator\Test;

require_once(__DIR__ . '/../MediatorInterface.php');
require_once(__DIR__ . '/../Chat.php');
require_once(__DIR__ . '/../ChatParts/AbstractUser.php');
require_once(__DIR__ . '/../ChatParts/Admin.php');
require_once(__DIR__ . '/../ChatParts/OrdinaryUser.php');

use shuravinGR\skillUp\Mediator\Chat;
use shuravinGR\SkillUp\Mediator\ChatParts\Admin;
use shuravinGR\SkillUp\Mediator\ChatParts\OrdinaryUser;

$admin = new Admin('Ban');

/** @var OrdinaryUser[] $users */
$users = [];
for ($i = 1; $i <= 12; $i++) {
    $users[] = new OrdinaryUser('userName' . $i);
}

$chat = new Chat($admin, $users);

$users[3]->sendMessage('Всем привет');

$newUser = new OrdinaryUser('newUser');
$chat->addUser($newUser);