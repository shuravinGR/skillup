<?php

namespace shuravinGR\SkillUp\Mediator\ChatParts;

class Admin extends AbstractUser
{
    public function getName(): string
    {
        return 'administrator_' . parent::getName();
    }

}