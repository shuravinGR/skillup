<?php

namespace shuravinGR\SkillUp\Mediator\ChatParts;

class OrdinaryUser extends AbstractUser
{
    public function getName(): string
    {
        return 'ordinaryUser_' . parent::getName();
    }

}