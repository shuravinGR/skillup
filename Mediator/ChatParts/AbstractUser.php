<?php

namespace shuravinGR\SkillUp\Mediator\ChatParts;

use shuravinGR\skillUp\Mediator\MediatorInterface;

abstract class AbstractUser
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var MediatorInterface|null
     */
    private ?MediatorInterface $chat = null;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }


    /**
     * @param string $message
     * @return void
     */
    public function sendMessage(string $message): void
    {
        if ($this->getChat() !== null) {
            $this->getChat()->sendMessage($message, $this);
        } else {
            echo 'Пользователь не состоит ни в одном чате!';
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return MediatorInterface|null
     */
    public function getChat(): ?MediatorInterface
    {
        return $this->chat;
    }

    /**
     * @param MediatorInterface $chat
     * @return AbstractUser
     */
    public function setChat(MediatorInterface $chat): AbstractUser
    {
        $this->chat = $chat;
        return $this;
    }
}