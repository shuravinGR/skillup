<?php

namespace shuravinGR\skillUp\Mediator;

use shuravinGR\SkillUp\Mediator\ChatParts\AbstractUser;

interface MediatorInterface
{
    /**
     * @param string $message
     * @return void
     */
    public function sendMessage(string $message, AbstractUser $user): void;
}