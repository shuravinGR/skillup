<?php

namespace shuravinGR\skillUp\Mediator;

use shuravinGR\SkillUp\Mediator\ChatParts\AbstractUser;
use shuravinGR\SkillUp\Mediator\ChatParts\Admin;
use shuravinGR\SkillUp\Mediator\ChatParts\OrdinaryUser;

class Chat implements MediatorInterface
{
    /**
     * @var Admin
     */
    private Admin $admin;

    /**
     * @var OrdinaryUser[]
     */
    private array $users = [];

    /**
     * @param Admin $admin
     * @param OrdinaryUser[] $users
     */
    public function __construct(Admin $admin, array $users = null)
    {
        $this->admin = $admin;
        $this->admin->setChat($this);

        if ($users !== null) {
            foreach ($users as $user) {
                $this->users[] = $user;
                $user->setChat($this);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function sendMessage(string $message, AbstractUser $user): void
    {
        if ($user instanceof Admin) {
            $this->publicMessage($user->getName(), $message);

            if ($this->getUsers() !== null) {
                foreach ($this->getUsers() as $chatUser) {
                    $this->publicMessage($chatUser->getName(), 'Славься великий админ!');
                }
            }
        } elseif ($user instanceof OrdinaryUser) {
            $this->publicMessage($user->getName(), 'Можно сообщение отправить?');
            $this->publicMessage($this->admin->getName(), 'Отправляй!');
            $this->publicMessage($user->getName(), $message);
        }
    }

    /**
     * @param string $userName
     * @param string $message
     * @return void
     */
    private function publicMessage(string $userName, string $message): void
    {
        echo $userName . ': ' . $message . "\n";
    }

    /**
     * @param OrdinaryUser $newUser
     * @return void
     */
    public function addUser(OrdinaryUser $newUser): void
    {
        $this->users[$newUser->getName()] = $newUser;

        $this->admin->sendMessage('В полку пользователей пополнение!');
    }

    /**
     * @return Admin
     */
    public function getAdmin(): Admin
    {
        return $this->admin;
    }

    /**
     * @return OrdinaryUser[]|null
     */
    public function getUsers(): ?array
    {
        return !empty($this->users) ? $this->users : null;
    }
}