<?php

namespace Builder;

class Table
{
    /**
     * @var array
     */
    private array $parts = [];

    /**
     * @return array
     */
    public function getParts(): array
    {
        return $this->parts;
    }

    public function addPart(string $part): void
    {
        $this->parts[] = $part;
    }
}