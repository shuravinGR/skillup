<?php

namespace Builder;

class WoodenTableBuilder implements BuilderInterface
{
    /**
     * @var Table
     */
    private Table $table;

    public function __construct()
    {
        $this->table = new Table();
    }

    public function addLegs(): void
    {
        $this->table->addPart('woodenLeg1');
        $this->table->addPart('woodenLeg2');
        $this->table->addPart('woodenLeg3');
        $this->table->addPart('woodenLeg4');
        $this->table->addPart('woodenLeg5');
        $this->table->addPart('woodenLeg6');
    }

    public function addTabletop(): void
    {
        $this->table->addPart('woodenTableTop');
    }

    public function getTable(): Table
    {
        echo "You have received a wooden table that contains:\n";

        foreach ($this->table->getParts() as $part) {
            echo $part . "\n";
        }

        return $this->table;
    }
}