<?php

namespace Builder;

class Director
{
    private BuilderInterface $builder;

    /**
     * @param BuilderInterface $builder
     */
    public function __construct(BuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    /**
     * @return BuilderInterface
     */
    private function getBuilder(): BuilderInterface
    {
        return $this->builder;
    }

    /**
     * @param BuilderInterface $builder
     * @return Director
     */
    public function setBuilder(BuilderInterface $builder): Director
    {
        $this->builder = $builder;
        return $this;
    }



    public function build(): Table
    {
        $builder = $this->getBuilder();

        $builder->addLegs();
        $builder->addTabletop();

        return $builder->getTable();
    }
}