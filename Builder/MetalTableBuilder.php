<?php

namespace Builder;

class MetalTableBuilder implements BuilderInterface
{
    /**
     * @var Table
     */
    private Table $table;

    public function __construct()
    {
        $this->table = new Table();
    }

    public function addLegs(): void
    {
        $this->table->addPart('metalLeg1');
        $this->table->addPart('metalLeg2');
        $this->table->addPart('metalLeg3');
        $this->table->addPart('metalLeg4');
    }

    public function addTabletop(): void
    {
        $this->table->addPart('metalTableTop');
    }

    public function getTable(): Table
    {
        echo "You have received a metal table that contains:\n";

        foreach ($this->table->getParts() as $part) {
            echo $part . "\n";
        }

        return $this->table;
    }
}