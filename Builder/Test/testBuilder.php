<?php
namespace Builder\Test;

use Builder\Director;
use Builder\MetalTableBuilder;
use Builder\WoodenTableBuilder;

require_once(__DIR__ . '/../BuilderInterface.php');
require_once(__DIR__ . '/../Director.php');
require_once(__DIR__ . '/../MetalTableBuilder.php');
require_once(__DIR__ . '/../Table.php');
require_once(__DIR__ . '/../WoodenTableBuilder.php');

$director = new Director(new MetalTableBuilder());
$director->build();

$director->setBuilder(new WoodenTableBuilder());
$director->build();