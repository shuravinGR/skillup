<?php

namespace Builder;

interface BuilderInterface
{
    public function addLegs();

    public function addTabletop();

    public function getTable();
}