<?php

namespace Singleton;

class MySingleton
{
    /**
     * @var self
     */
    private static self $instance;

    /**
     * @var string
     */
    private static string $someVariable = 'defaultValue';

    public function __construct()
    {
    }

    /**
     * @return self
     */
    public static function getInstance() : self
    {
        return self::$instance ?? new self;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __sleep()
    {
    }

    /**
     * @return string
     */
    public function getSomeVariable(): string
    {
        return self::$someVariable;
    }

    /**
     * @param string $someVariable
     */
    public function setSomeVariable(string $someVariable): void
    {
        self::$someVariable = $someVariable;
    }
}