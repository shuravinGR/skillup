<?php

namespace Singleton;

require_once __DIR__ . '/MySingleton.php';

$obj1 = MySingleton::getInstance();
echo $obj1->getSomeVariable() . "\n";
$obj1->setSomeVariable('notDefaultValue');
echo $obj1->getSomeVariable() . "\n";

$obj2 = MySingleton::getInstance();
echo $obj2->getSomeVariable() . "\n";
$obj2->setSomeVariable('new not default value');

echo $obj1->getSomeVariable() . "\n";
echo $obj2->getSomeVariable() . "\n";