<?php

namespace Iterator;

interface IteratorInterface
{
    public function current();

    public function next();

    public function hasNext();

    public function prev();

    public function hasPrev();

    public function key();

    public function rewind();
}