<?php

namespace Iterator;

class Iterator implements IteratorInterface
{
    /**
     * @var array
     */
    private array $arr;

    /**
     * @var int
     */
    private int $pos = 0;

    /**
     * @param array $arr
     */
    public function __construct(array $arr)
    {
        $this->arr = $arr;
    }

    public function current()
    {
        $this->makeEcho($this->arr[$this->pos] ?? "here's nothing");
    }

    public function next()
    {
        if ($this->hasNext()) {
            $this->pos++;
            $this->current();
        } else {
            $this->makeEcho("here's nothing");
        }
    }

    /**
     * @return bool
     */
    public function hasNext(): bool
    {
        $this->makeEcho(($result = isset($this->arr[$this->pos + 1])) ? 'next is exist' : 'next does not exist');
        return $result;
    }

    public function prev()
    {
        if ($this->hasPrev()) {
            $this->pos--;
            $this->current();
        } else {
            $this->makeEcho("here's nothing");
        }
    }

    /**
     * @return bool
     */
    public function hasPrev(): bool
    {
        $this->makeEcho(($result = isset($this->arr[$this->pos - 1])) ? 'previous is exist' : 'previous does not exist');
        return $result;
    }

    public function key()
    {
        $this->makeEcho($this->pos);
    }

    public function rewind()
    {
        $this->makeEcho('rewind complete');
        return $this->pos = 0;
    }

    private function makeEcho($result)
    {
        echo $result . "\n";
    }
}