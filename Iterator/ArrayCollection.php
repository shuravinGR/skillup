<?php

namespace Iterator;

class ArrayCollection
{
    /**
     * @var array
     */
    protected array $collection;

    /**
     * @var Iterator
     */
    protected Iterator $iterator;

    /**
     * @param array $collection
     */
    public function __construct(array $collection)
    {
        $this->collection = $collection;
        $this->iterator = new Iterator($this->collection);
    }

    /**
     * @return Iterator
     */
    public function getIterator(): Iterator
    {
        return $this->iterator;
    }
}