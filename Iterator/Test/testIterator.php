<?php

namespace Iterator\Test;

use Iterator\ArrayCollection;

require_once(__DIR__ . '/../IteratorInterface.php');
require_once(__DIR__ . '/../Iterator.php');
require_once(__DIR__ . '/../ArrayCollection.php');

$myCollection = new ArrayCollection(['first item', 'second item', 'third item', 'fourth item', 'fifth item', 'sixth item']);
$iterator = $myCollection->getIterator();

$iterator->key();
$iterator->hasPrev();
$iterator->current();
$iterator->hasNext();
$iterator->next();
$iterator->next();
$iterator->next();
$iterator->next();
$iterator->next();
$iterator->next();
$iterator->hasPrev();
$iterator->rewind();
$iterator->current();
$iterator->next();
$iterator->next();
$iterator->prev();