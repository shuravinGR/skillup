<?php

namespace shuravinGR\SkillUp\Strategy;

use shuravinGR\SkillUp\Strategy\Strategies\Addition;
use shuravinGR\SkillUp\Strategy\Strategies\Division;
use shuravinGR\SkillUp\Strategy\Strategies\Multiplication;
use shuravinGR\SkillUp\Strategy\Strategies\Subtraction;

class Calculator
{
    private Executor $executor;

    public function __construct()
    {
        $this->executor = new Executor();
    }

    /**
     * @param int $a
     * @param int $b
     * @param string $strategy
     * @return void
     */
    public function calculate(int $a, int $b, string $strategy)
    {
        switch ($strategy) {
            case 'addition':
                $strategyEntity = new Addition();
                break;
            case 'division':
                $strategyEntity = new Division();
                break;
            case 'multiplication':
                $strategyEntity = new Multiplication();
                break;
            case 'subtraction':
                $strategyEntity = new Subtraction();
                break;
            default:
                $strategyEntity = new Addition();
        }

        $this->executor->setStrategy($strategyEntity);
        echo $strategy . ': ' . $this->executor->count($a, $b) . "\n";
    }
}