<?php

namespace shuravinGR\SkillUp\Strategy;

use shuravinGR\SkillUp\Strategy\Strategies\StrategyInterface;

class Executor
{
    private StrategyInterface $strategy;

    /**
     * @param int $a
     * @param int $b
     */
    public function count($a, $b)
    {
        return $this->getStrategy()->execute($a, $b);
    }

    /**
     * @return StrategyInterface
     */
    public function getStrategy(): StrategyInterface
    {
        return $this->strategy;
    }

    /**
     * @param StrategyInterface $strategy
     */
    public function setStrategy(StrategyInterface $strategy): void
    {
        $this->strategy = $strategy;
    }
}