<?php

namespace shuravinGR\SkillUp\Strategy\Strategies;

class Addition implements StrategyInterface
{
    public function execute($a, $b)
    {
        return $a + $b;
    }
}