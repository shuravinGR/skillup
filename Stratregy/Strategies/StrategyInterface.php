<?php

namespace shuravinGR\SkillUp\Strategy\Strategies;

interface StrategyInterface
{
    public function execute($a, $b);
}