<?php

namespace shuravinGR\SkillUp\Strategy\Strategies;

class Subtraction implements StrategyInterface
{
    public function execute($a, $b)
    {
        return $a - $b;
    }
}