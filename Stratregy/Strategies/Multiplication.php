<?php

namespace shuravinGR\SkillUp\Strategy\Strategies;

class Multiplication implements StrategyInterface
{
    public function execute($a, $b)
    {
        return $a * $b;
    }
}