<?php

namespace shuravinGR\SkillUp\Strategy\Strategies;

class Division implements StrategyInterface
{
    public function execute($a, $b)
    {
        return $a / $b;
    }
}