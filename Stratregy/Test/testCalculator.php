<?php

namespace shuravinGR\SkillUp\Stratregy\Test;

use shuravinGR\SkillUp\Strategy\Calculator;

require_once __DIR__ . '/../Calculator.php';
require_once __DIR__ . '/../Executor.php';
require_once __DIR__ . '/../Strategies/StrategyInterface.php';
require_once __DIR__ . '/../Strategies/Addition.php';
require_once __DIR__ . '/../Strategies/Division.php';
require_once __DIR__ . '/../Strategies/Multiplication.php';
require_once __DIR__ . '/../Strategies/Subtraction.php';

$calculator = new Calculator();

$calculator->calculate(1, 2, 'addition');
$calculator->calculate(1, 2, 'subtraction');
$calculator->calculate(1, 2, 'multiplication');
$calculator->calculate(1, 2, 'division');