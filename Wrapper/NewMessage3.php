<?php

namespace Wrapper;

class NewMessage3 extends NewMessage2
{
    public function getMessage(): string
    {
        return 'added from newMassage3 ' . parent::getMessage();
    }

}