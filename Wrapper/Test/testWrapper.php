<?php

namespace Wrapper\Test;

use Wrapper\MainMessage;
use Wrapper\NewMessage1;
use Wrapper\NewMessage2;
use Wrapper\NewMessage3;

require_once( __DIR__ . '/../MainMessage.php');
require_once( __DIR__ . '/../NewMessage1.php');
require_once( __DIR__ . '/../NewMessage2.php');
require_once(__DIR__ . '/../NewMessage3.php');

echo (new MainMessage())->getMessage() . "\n";
echo (new NewMessage1())->getMessage() . "\n";
echo (new NewMessage2())->getMessage() . "\n";
echo (new NewMessage3())->getMessage() . "\n";