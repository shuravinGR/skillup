<?php

namespace shuravinGR\SkillUp\Traits;

trait SayHelloTrait
{
    public function sayHello()
    {
        echo "Hello!\n";
    }
}