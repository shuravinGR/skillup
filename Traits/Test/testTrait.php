<?php
namespace shuravinGR\SkillUp\Traits\Test;

use shuravinGR\SkillUp\Traits\User;

require_once(__DIR__ . '/../SayHelloTrait.php');
require_once(__DIR__ . '/../SayHiTrait.php');
require_once(__DIR__ . '/../User.php');

$user = new User();
$user->sayHello();
$user->sayHi();