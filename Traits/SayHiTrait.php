<?php

namespace shuravinGR\SkillUp\Traits;

trait SayHiTrait
{
    public function sayHi()
    {
        echo "Hi!\n";
    }
}