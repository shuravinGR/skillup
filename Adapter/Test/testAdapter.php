<?php

namespace Adapter\Test;

use Adapter\Calculator;
use Adapter\StringCalculator;
use Adapter\StringCalculatorAdapter;

require_once( __DIR__ . '/../CalculatorInterface.php');
require_once( __DIR__ . '/../Calculator.php');
require_once( __DIR__ . '/../StringCalculatorInterface.php');
require_once( __DIR__ . '/../StringCalculator.php');
require_once( __DIR__ . '/../StringCalculatorAdapter.php');

echo "Numeric calculator:\n";
$numericCalculator = new Calculator();
echo $numericCalculator->addition(1,2) . "\n";
echo $numericCalculator->subtraction(3,2) . "\n";

echo "String calculator:\n";
$stringCalculator = new StringCalculator();
echo $stringCalculator->addition('1', '2') . "\n";
echo $stringCalculator->subtraction('3', '2') . "\n";

echo "String calculator adapter:\n";
$stringCalculatorAdapter = new StringCalculatorAdapter();
$stringCalculatorAdapter->setStringCalculator($stringCalculator);
echo $stringCalculatorAdapter->addition(1, 2) . "\n";
echo $stringCalculatorAdapter->subtraction(3, 2) . "\n";