<?php

namespace Adapter;

class Calculator implements CalculatorInterface
{

    /**
     * @param int $x
     * @param int $y
     * @return int
     */
    public function addition($x, $y): int
    {
        return $x + $y;
    }

    /**
     * @param int $x
     * @param int $y
     * @return int
     */
    public function subtraction($x, $y): int
    {
        return $x - $y;
    }
}