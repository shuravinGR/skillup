<?php

namespace Adapter;

class StringCalculatorAdapter implements CalculatorInterface
{
    private StringCalculatorInterface $stringCalculator;

    /**
     * @param StringCalculatorInterface $stringCalculator
     * @return StringCalculatorAdapter
     */
    public function setStringCalculator(StringCalculatorInterface $stringCalculator): StringCalculatorAdapter
    {
        $this->stringCalculator = $stringCalculator;
        return $this;
    }

    /**
     * @param int $x
     * @param int $y
     * @return int
     */
    public function addition($x, $y): int
    {
        return (int)$this->stringCalculator->addition((string)$x, (string)$y);
    }

    /**
     * @param int $x
     * @param int $y
     * @return int
     */
    public function subtraction($x, $y): int
    {
        return (int)$this->stringCalculator->subtraction((string)$x, (string)$y);
    }
}