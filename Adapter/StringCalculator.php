<?php

namespace Adapter;

class StringCalculator implements StringCalculatorInterface
{

    /**
     * @param string $x
     * @param string $y
     * @return string
     */
    public function addition($x, $y): string
    {
        return (string)((int)$x + (int)$y);
    }

    /**
     * @param string $x
     * @param string $y
     * @return string
     */
    public function subtraction($x, $y): string
    {
        return (string)((int)$x - (int)$y);
    }
}