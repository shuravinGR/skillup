<?php

namespace Adapter;

interface StringCalculatorInterface
{
    /**
     * @param $x
     * @param $y
     * @return string
     */
    public function addition($x, $y): string;

    /**
     * @param $x
     * @param $y
     * @return string
     */
    public function subtraction($x, $y): string;
}