<?php

namespace Adapter;

interface CalculatorInterface
{
    /**
     * @param $x
     * @param $y
     * @return int
     */
    public function addition($x, $y): int;

    /**
     * @param $x
     * @param $y
     * @return int
     */
    public function subtraction($x, $y): int;
}