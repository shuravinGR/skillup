<?php

namespace PHPMagicMethods;

require_once __DIR__ . '/TestClass.php';
require_once __DIR__ . '/ChildTestClass.php';

echo "constructor test\n";
$testClass = new TestClass('test');
$childTestClass = new ChildTestClass('childTest');

echo "\ngetters and setters test\n";
echo $testClass->name;
$testClass->name = 'test value';

echo "\nisset test\n";
isset($testClass->name);

echo "\nunset test\n";
unset($testClass->name);

echo "\ntoString test\n";
echo $testClass;
(string)$testClass;

echo "\nserialize test\n";
$serialized = serialize($testClass);
unserialize($serialized);
$serialized = serialize($childTestClass);
unserialize($serialized);

echo "\n__call test\n";
$testClass->protectedMethod('test params');
$testClass::protectedMethod('test params for static');

echo "\nclone test\n";
$clonedTestClass = clone $testClass;

echo "\ninvoke test\n";
$testClass();