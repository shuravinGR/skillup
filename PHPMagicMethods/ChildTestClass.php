<?php

namespace PHPMagicMethods;

class ChildTestClass extends TestClass
{
    public function __construct(string $name)
    {
        parent::__construct($name);
        echo "Child Test Class constructor\n";
    }

    public function __serialize()
    {
        echo "была попытка сериализации класса(__serialize)\n";
        return [
            'name' => $this->name,
        ];
    }

    public function __unserialize(array $data)
    {
        echo "была попытка десериализации класса(__unserialize)\n";
        $this->name = $data['name'];
    }
}