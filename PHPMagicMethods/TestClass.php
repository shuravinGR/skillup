<?php

namespace PHPMagicMethods;

class TestClass
{
    protected string $name;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        echo "Test Class constructor\n";
    }

    public function __get($name)
    {
        echo "была попытка получить значение из свойства $name\n";
    }

    public function __set($name, $val)
    {
        echo "была попытка установить значение $val в свойство $name\n";
    }

    public function __isset($name)
    {
        echo "была попытка проверки на существование свойства $name\n";
    }

    public function __unset($name)
    {
        echo "была попытка очистики значения свойства $name\n";
    }

    public function __toString()
    {
        echo "была попытка привести класс к строке\n";
        return "testClass\n";
    }

    public function __sleep()
    {
        echo "была попытка сериализации класса(__sleep)\n";
        return ['name'];
    }

    public function __wakeup()
    {
        echo "была попытка десериализации класса(__wakeup)\n";
    }

    public function __call(string $name, array $params)
    {
        $parameters = '';
        foreach ($params as $param) {
            $parameters .= $param . ', ';
        }
        echo "была попытка вызова метода $name с передачей параметров $parameters\n";
    }

    public static function __callStatic(string $name, array $params)
    {
        $parameters = '';
        foreach ($params as $param) {
            $parameters .= $param . ', ';
        }
        echo "была попытка вызова статического метода $name с передачей параметров $parameters\n";
    }

    public function __clone()
    {
        echo "была попытка клонирования объекта\n";
    }

    public function __invoke()
    {
        echo "была попытка использования объекта как функции\n";
    }
}