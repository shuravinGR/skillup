<?php

namespace Observer;

interface ObserverInterface
{
    /**
     * @param SubscriberInterface $subscriber
     */
    public function subscribe(SubscriberInterface $subscriber);

    /**
     * @param SubscriberInterface $subscriber
     */
    public function unsubscribe(SubscriberInterface $subscriber);

    /**
     * @param Product $product
     */
    public function addNewProduct(Product $product);

    public function notifySubscribers(Product $product);

}