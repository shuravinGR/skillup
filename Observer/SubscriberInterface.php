<?php

namespace Observer;

interface SubscriberInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param Product $product
     */
    public function notify(Product $product);
}