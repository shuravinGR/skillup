<?php

namespace Observer;

class Store implements ObserverInterface
{
    /**
     * @var SubscriberInterface[]
     */
    private array $subscribers;

    /**
     * @var Product[]
     */
    private array $products = [];

    /**
     * @param SubscriberInterface $subscriber
     * @return void
     */
    public function subscribe(SubscriberInterface $subscriber)
    {
        $this->subscribers[$subscriber->getName()] = $subscriber;
        echo 'Добавлен новый подписчик ' . $subscriber->getName() . "!\n";
        foreach ($this->products as $product) {
            $subscriber->notify($product);
        }
    }

    /**
     * @param SubscriberInterface $subscriber
     * @return void
     */
    public function unsubscribe(SubscriberInterface $subscriber)
    {
        if (array_key_exists($subscriber->getName(), $this->subscribers)) {
            unset($this->subscribers[$subscriber->getName()]);
        }
        echo 'Подписчик ' . $subscriber->getName() . " отказался от подписки!\n";
    }

    /**
     * @param Product $product
     * @return void
     */
    public function addNewProduct(Product $product)
    {
        $this->products[$product->getName()] = $product;
        echo 'Новый товар ' . $product->getName() . " появился в наличии!\n";
        $this->notifySubscribers($product);
        echo "Все подписчики уведомлены!\n";
    }

    /**
     * @param Product $product
     * @return void
     */
    public function notifySubscribers(Product $product)
    {
        foreach ($this->subscribers as $subscriber) {
            $subscriber->notify($product);
        }
    }
}