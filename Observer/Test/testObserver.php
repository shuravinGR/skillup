<?php

namespace Observer\Test;

use Observer\Client\RegularClient;
use Observer\Client\UsualClient;
use Observer\Product;
use Observer\Store;

require_once __DIR__ . '/../ObserverInterface.php';
require_once __DIR__ . '/../SubscriberInterface.php';
require_once __DIR__ . '/../Product.php';
require_once __DIR__ . '/../Store.php';
require_once __DIR__ . '/../Client/AbstractClient.php';
require_once __DIR__ . '/../Client/RegularClient.php';
require_once __DIR__ . '/../Client/UsualClient.php';

$store = new Store();
$vasya = new UsualClient('Вася');
$kolya = new UsualClient('Коля');
$kostya = new RegularClient('Костя');

$smartphone = new Product('iPhone 14 Pro Max');
$notebook = new Product('Lenovo Yoga');
$watch = new Product('Rolex Submariner');

$store->subscribe($vasya);
$store->subscribe($kolya);
$store->subscribe($kostya);

$vasya->addToFavourites($notebook);
$vasya->addToFavourites($watch);

$kolya->addToFavourites($smartphone);
$kolya->addToFavourites($watch);

$kostya->addToFavourites($smartphone);
$kostya->addToFavourites($notebook);
$kostya->addToFavourites($watch);

$store->addNewProduct($notebook);
$vasya->buyProduct($notebook);
$store->unsubscribe($vasya);
$store->addNewProduct($watch);
$kolya->buyProduct($watch);
$store->addNewProduct($smartphone);
$store->subscribe($vasya);
$vasya->buyProduct($watch);