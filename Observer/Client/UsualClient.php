<?php

namespace Observer\Client;

use Observer\Product;

class UsualClient extends AbstractClient
{

    /**
     * @inheritDoc
     */
    public function notify(Product $product)
    {
        if (array_key_exists($product->getName(), $this->favourites)) {
            echo 'Клиент ' . $this->getName() . ' получил уведомление о поступлении товара ' . $product->getName() . "!\n";
        }
    }

    public function buyProduct(Product $product)
    {
        echo 'Клиент ' . $this->getName() . ' купил товар ' . $product->getName() . "!\n";
        $this->removeFromFavourites($product);
    }
}