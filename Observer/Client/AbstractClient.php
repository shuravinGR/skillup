<?php

namespace Observer\Client;

use Observer\Product;
use Observer\SubscriberInterface;

abstract class AbstractClient implements SubscriberInterface
{
    /**
     * @var string
     */
    protected string $name;

    /**
     * @var Product[]
     */
    protected array $favourites;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Product $product
     * @return void
     */
    public function addToFavourites(Product $product)
    {
        $this->favourites[$product->getName()] = $product;
    }

    public function removeFromFavourites(Product $product)
    {
        if (array_key_exists($product->getName(), $this->favourites))
        {
            unset($this->favourites[$product->getName()]);
            echo 'Клиент ' . $this->getName() . ' удалил товар ' . $product->getName() . " из избранного!\n";
        }
    }

    /**
     * @inheritDoc
     */
    public abstract function notify(Product $product);
}