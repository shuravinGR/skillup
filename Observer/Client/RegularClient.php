<?php

namespace Observer\Client;

use Observer\Product;

class RegularClient extends AbstractClient
{
    /**
     * @inheritDoc
     */
    public function notify(Product $product)
    {
        if (array_key_exists($product->getName(), $this->favourites)) {
            echo 'Клиент ' . $this->getName() . ' получил уведомление о поступлении товара ' . $product->getName() . " и сразу же купил его!\n";
            $this->removeFromFavourites($product);
        }
    }
}